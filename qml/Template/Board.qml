import QtQuick 2.0
import "MapHelper.js" as MapHelper

GridView {
    property variant selectedBox: undefined

    function selectBox(box)
    {
        var handled = false;

        if(selectedBox === undefined) {
            selectedBox = box;
            handled = true;
        }
        else {
            selectedBox.isSelected = false;

            if(MapHelper.isNeighbour(selectedBox, box)) {
                MapHelper.solveBoxes(selectedBox, box, boardModel)
                selectedBox = undefined
            }
            else {
                selectedBox = undefined
            }
        }

        return handled;
    }

    Component.onCompleted: {
        MapHelper.generateMap(boardModel);
    }

    ListModel {
        id: boardModel
    }

    id: board
    anchors.fill: parent

    cellWidth: parent.width / MapHelper.mapSize
    cellHeight: parent.width / MapHelper.mapSize

    model: boardModel
    delegate: Box {
        width: board.cellWidth - 2
        height: board.cellHeight - 2
    }
}
