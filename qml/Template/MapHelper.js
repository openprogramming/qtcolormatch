var mapSize = 10;

function generateMap(listModel) {
    var colorArray = createColorArray();

    listModel.clear();
    for(var i = 0; i < mapSize * mapSize; ++i) {

        var randIdx = Math.floor(Math.random() * 4.99);
        var randColor = colorArray[randIdx];

        var modelDict = {
            "boxColor":randColor,
            "xPos":getXForIdx(i),
            "yPos":getYForIdx(i)
        }

        listModel.append(modelDict);
    }
}

function createColorArray()
{
    var array = ["red", "green", "blue", "yellow", "orange"];
    return array;
}

function getXForIdx(idx)
{
    return idx % mapSize;
}

function getYForIdx(idx)
{
    return Math.floor(idx / mapSize)
}

function getIdxFor(x, y)
{
    return y * mapSize + x;
}

function isNeighbour(box1, box2)
{
    var isHorizontalNeighbour = box1.yIdx === box2.yIdx &&
            (box1.xIdx - 1 === box2.xIdx || box1.xIdx + 1 === box2.xIdx  );
    var isVerticalNeighbour = box1.xIdx === box2.xIdx &&
            (box1.yIdx - 1 === box2.yIdx || box1.yIdx + 1 === box2.yIdx  );

    return isHorizontalNeighbour || isVerticalNeighbour;
}

function solveBoxes(box1, box2, modelList)
{
    var color1 = getBoxForPosition(box1.xIdx, box1.yIdx, modelList).boxColor;
    var color2 = getBoxForPosition(box2.xIdx, box2.yIdx, modelList).boxColor;
    setBoxForPosition({"boxColor":color2}, box1.xIdx, box1.yIdx, modelList);
    setBoxForPosition({"boxColor":color1}, box2.xIdx, box2.yIdx, modelList);
}

function solveBoxForPosition(box, x, y, modelList)
{
    var cnt = 0;

    var nextBoxModel = getBoxForPosition(x + 1, y, modelList);
    while(nextBoxModel.boxColor === box.colorStr && nextBoxModel.xPos < 10)
    {
        ++cnt;
        nextBoxModel = getBoxForPosition(x + cnt, y, modelList)
    }
    console.log(cnt)
}

function getBoxForPosition(x, y, listModel)
{
    return listModel.get(getIdxFor(x, y));
}

function setBoxForPosition(boxModel, x, y, listModel)
{
    listModel.set(getIdxFor(x, y), boxModel);
}
