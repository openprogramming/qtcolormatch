import QtQuick 2.0

Rectangle{
    id: box
    property bool isSelected

    property int xIdx: xPos;
    property int yIdx: yPos;
    property string colorStr: boxColor

    border.color: "black"
    border.width: 1
    radius: 3

    color: boxColor

    MouseArea {
        anchors.fill: parent
        onClicked: {
            var handled = board.selectBox(box);
            box.isSelected = handled;
        }
    }

    states: [
        State {
            name:"selected"
            when: box.isSelected
            PropertyChanges {
                target: box; scale: 1.1
            }
        }
    ]
}
